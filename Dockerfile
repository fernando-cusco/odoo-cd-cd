FROM odoo:12

#copiamos el archivo de configuración de odoo a odoo base
COPY odoo.conf /etc/odoo

ADD extra-addons /mnt/extra-addons

EXPOSE 8069